package ej_parcial;

import java.util.Stack;

public class Agenda 
{
	private Stack<Contacto> agenda;
	
	public Agenda()
	{
		agenda = new Stack<Contacto>();
	}
	
	public void agregar(Integer nro, String nombre)
	{
		Stack<Contacto> temp= new Stack<Contacto>();
		Contacto c = new Contacto(nro, nombre);
		
		if (agenda.isEmpty())
		{
			agenda.push(c);
			return;
		}
			
		while(!agenda.isEmpty() && c.getNumero() < agenda.peek().getNumero())
		{
			temp.push(agenda.peek());
			agenda.pop();
		}
		
		agenda.push(c);
		
		while(!temp.isEmpty())
		{
			agenda.push(temp.peek());
			temp.pop();
		}
		
	}
	
	public String toString()
	{
		int i = -1;
		
		return toString(i);
	}
	
	private String toString(int i)
	{
		if(i == agenda.size()-1)
		{
			return " ";
		}
		
		else
		{
			i++;
			
			return agenda.get(i).toString() + " " + toString(i);
		}
	}
	
//	public String toString()
//	{
//		String s = "";
//		
//		Stack<Contacto> aux = new Stack<>();
//		
//		aux.addAll(this.agenda);
//		
//		while(!aux.isEmpty())
//			s = s + " " +aux.pop().toString();
//		
//		return s;
//	}
	
	
	public static void main(String[] args) {
		Agenda a = new Agenda();
		
		a.agregar(1,"holo");
		a.agregar(3, "nombre");
		a.agregar(4, "otro_nombre");
		a.agregar(2, "sdfg");
		
		System.out.println(a.toString());
	}
}
