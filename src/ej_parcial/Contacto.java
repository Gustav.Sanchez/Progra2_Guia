package ej_parcial;

public class Contacto 
{
	private Integer numero;
	private String nombre;
	
	public Contacto(Integer nro, String nombre)
	{
		this.numero = nro;
		this.nombre = nombre;
	}
	
	public String toString()
	{
		return "{" + this.numero + ",\""+ this.nombre + "\"}";
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public Integer getNumero()
	{
		return this.numero;
	}
}
