package prog2.prac2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import prog2.prac2.Agregados;
import prog2.prac2.EliminarDuplicados;
import prog2.prac2.Partido;
import prog2.prac2.Persona;


@RunWith(Enclosed.class)
public class TestPrac2
{
	//
	// Tests para EliminarDuplicados.
	//
	public static class TestEliminarDuplicados
	{
		private static final String[] elemsConsigna =
			{"X", "Z", "A", "Z", "B", "Y", "B"};

		@Test
		public void ejemploConsigna() {
			LinkedList<String> l = new LinkedList<>(Arrays.asList(elemsConsigna));
			LinkedList<String> expected = new LinkedList<>(Arrays.asList(
					new String[] {"X", "Z", "A", "B", "Y"}));

			assertEquals(expected, EliminarDuplicados.elimDuplicados(l));
		}

		@Test
		public void ejemploConsignaIzq() {
			ArrayList<String> l = new ArrayList<>(Arrays.asList(elemsConsigna));
			ArrayList<String> expected = new ArrayList<>(Arrays.asList(
					new String[] {"X", "A", "Z", "Y", "B"}));

			assertEquals(expected, EliminarDuplicados.elimDuplicadosIzq(l));
		}
	}

	//
	// Tests para ganadorLiga().
	//
	public static class TestLiga
	{
		@Test
		public void unPartido() {
			List<Partido> l = Arrays.asList(new Partido[] {
					new Partido("Eq 1", 3, "Eq 2", 4),
			});
			assertEquals("Eq 2", Agregados.ganadorLiga(l));
		}
	}

	//
	// Tests para agruparCumple().
	//
	public static class TestCumples
	{
		@Test
		public void ejemploConsigna() {
			List<Persona> l = Arrays.asList(new Persona[] {
					new Persona("Juan", "27/05/1993"),
					new Persona("Alicia", "30/11/1990"),
					new Persona("Elena", "27/05/1985"),
			});
			Map<String, List<String>> cumples = Agregados.agruparCumple(l);

			assertEquals(2, cumples.size());
			assertTrue(cumples.containsKey("30/11"));
			assertTrue(cumples.containsKey("27/05"));

			assertEquals(
					new HashSet<>(Arrays.asList(new String[] {"Alicia"})),
					new HashSet<>(cumples.get("30/11")));

			assertEquals(
					new HashSet<>(Arrays.asList(new String[] {"Juan", "Elena"})),
					new HashSet<>(cumples.get("27/05")));
		}
	}
	
}