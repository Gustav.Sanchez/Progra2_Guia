package prog2.prac2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class EliminarDuplicados 
{
	public static LinkedList<String> elimDuplicados(LinkedList<String> lista)
	{
		LinkedList<String> sinRepetidos = new LinkedList<String>();
		HashMap<String,String> dic = new HashMap<String,String>();
		
		for(String i : lista)
		{
			if(!dic.containsKey(i))
				sinRepetidos.add(i);
			
			dic.put(i, i);
		}

		return sinRepetidos;
	}

	public static  ArrayList<String> elimDuplicadosIzq(ArrayList<String> lista) 
	{
		ArrayList<String> sinRepetidosIzq = new ArrayList<String>();
		HashMap<String,Integer> dic = new HashMap<String,Integer>();
		Integer val = null;
		
		for(String x: lista)
		{
			if(!dic.containsKey(x))
				dic.put(x,1);
			
			else
			{
				val = dic.get(x).intValue() +1;
				dic.put(x, val);
			}				
		}
		
		for(String x: lista)
		{
			if(dic.containsKey(x) && dic.get(x) == 1)
				sinRepetidosIzq.add(x);
			
			else if(dic.containsKey(x) && dic.get(x) >1)
			{
				val = dic.get(x).intValue() -1;
				dic.put(x, val);	
			}
		}
		
		return sinRepetidosIzq;
	}
}
