package prog2.prac2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Agregados 
{

	public static String ganadorLiga(List<Partido> l)
	{
		Integer val = null;
		String campeon = null;
		Integer puntosCampeon = 0;
		HashMap<String,Integer> tablaEquipos = new HashMap<String,Integer>();
		
		for(Partido p : l)
		{
			tablaEquipos.put(p.equipo1, 0);
			tablaEquipos.put(p.equipo2, 0);
		}
		
		for(Partido p : l)
		{
			if(p.equipo1.equals(ganadorPartido(p)))
			{
				val = tablaEquipos.get(p.equipo1) + 2;
				tablaEquipos.put(p.equipo1, val);
			}
			
			else if (p.equipo2.equals(ganadorPartido(p)))
			{
				val = tablaEquipos.get(p.equipo2) + 2;
				tablaEquipos.put(p.equipo2, val);
			}
			
			else
			{
				val = tablaEquipos.get(p.equipo1) + 1;
				tablaEquipos.put(p.equipo1, val);
				
				val = tablaEquipos.get(p.equipo2) + 1;
				tablaEquipos.put(p.equipo2, val);
			}
		}
		
		for (Entry<String, Integer> entry : tablaEquipos.entrySet())
		{
			
			if(puntosCampeon < entry.getValue())
			{
				puntosCampeon = entry.getValue();
				campeon = entry.getKey();
			}
		}
		
		return campeon;
		
	}

	private static String ganadorPartido(Partido p)
	{
		if (p.goles1>p.goles2)
			return p.equipo1;
		
		else if (p.goles1<p.goles2)
			return p.equipo2;
		
		else
			return "empate";
	}
	
	public static Map<String, List<String>> agruparCumple(List<Persona> l)
	{
		HashMap<String,List<String>> grupos = new HashMap<String, List<String>>();
		
		for(Persona p: l)
		{
			if(!grupos.containsKey(fechaCorta(p.fechaNac)))
				grupos.put(fechaCorta(p.fechaNac), new ArrayList<String>());
		}
		
		for(Persona p: l)
		{
			if(grupos.containsKey(fechaCorta(p.fechaNac)))
				grupos.get(fechaCorta(p.fechaNac)).add(p.nombre);
		}
	
		return grupos;
	}
	
	private static String fechaCorta(String ddmmaaa)
	{
		return ddmmaaa.substring(0, 5);
	}
}
