package prog2.arboles;

public class ABB
{
	protected int tam;
	protected static Nodo raiz;

	protected static class Nodo {
		public int val;
		public Nodo izq, der;
		public Nodo(int v) { val = v; }
	}

	public boolean pertenece(int x) {
		return pertenece(raiz, x);
	}

	public int cantidad() {
		return tam;
	}

	public void insertar(int x) {
		raiz = insertar(raiz, x);
	}

	public void eliminar(int x) {
		raiz = eliminar(raiz, x);
	}

	@Override
	public String toString() {
		return "AB<" + toString(raiz) + ">";
	}

	//
	// Métodos auxiliares.
	//

	protected boolean pertenece(Nodo p, int x) {
		if(p == null)
			return false;

		if (x == p.val)
			return true;

		if (x < p.val)
			return pertenece(p.izq, x);
		else
			return pertenece(p.der, x);
	}

	protected Nodo insertar(Nodo p, int x) {
		if (p == null) {
			tam++;
			return new Nodo(x);
		}

		if (x < p.val)
			p.izq = insertar(p.izq, x);

		else if (x > p.val)
			p.der = insertar(p.der, x);

		return p;  // No se admiten duplicados.
	}

	protected Nodo eliminar(Nodo p, int x) {
		if (p == null)
			return null;

		if (x < p.val)
			// Intentamos eliminar por la izquierda.
			p.izq = eliminar(p.izq, x);

		else if (x > p.val)
			// Intentamos eliminar por la derecha.
			p.der = eliminar(p.der, x);

		else {
			// x == p.val: borraremos el nodo.
			tam--;

			// Si "p" es hoja o solo tiene un hijo, es fácil: sin
			// recursión, se devuelve directamente la otra rama, o null.
			if (p.izq == null && p.der == null)
				return null;

			if (p.izq == null)
				return p.der;

			if (p.der == null)
				return p.izq;

			// Caso difícil: "p" tiene dos hijos. Buscamos el mayor
			// descendiente por la izquierda (podría ser también el
			// menor por la derecha).
			p.val = maxVal(p.izq);

			// Para borrar ese "mayor descendiente por la izquierda",
			// invocamos recursivamente *una vez*.
			p.izq = eliminar(p.izq, p.val);
		}

		return p;
	}

	private int maxVal(Nodo p) {
		// Pre-condición: p != null.
		while (p.der != null)
			p = p.der;
		return p.val;
	}

	private String toString(Nodo p) {
		if (p == null)
			return "\u2205";  // Símbolo de vacío.

		String val = String.valueOf(p.val);
		if (esHoja(p))
			return val;  // La hoja se representa a sí misma.

		return "(" + val + " " + toString(p.izq) + " " + toString(p.der) + ")";
	}

	private boolean esHoja(Nodo p) {
		return (p.izq == null && p.der == null);
	}
}