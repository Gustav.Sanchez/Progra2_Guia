package prog2.prac3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class AbbAlumno
{
	
	protected int tam;
	protected static Nodo raiz;

	protected static class Nodo 
	{
		public int val;
		public Nodo izq, der;
		public Nodo(int v) { val = v; }
	}
	
	public String toString() {
		return "AB<" + toString(raiz) + ">";
	}
	
	private String toString(Nodo p) {
		if (p == null)
			return "\u2205";  // Simbolo de vacío.

		String val = String.valueOf(p.val);
		if (esHoja(p))
			return val;  // La hoja se representa a sí misma.

		return "(" + val + " " + toString(p.izq) + " " + toString(p.der) + ")";
	}
	
	public void insertar(Integer x) 
	{
		
		raiz = insertar(raiz, x);
	}
	
	protected Nodo insertar(Nodo p, Integer x) 
	{
		if (p == null) {
			tam++;
			return new Nodo(x);
		}

		if (x < p.val)
			p.izq = insertar(p.izq, x);

		else if (x> p.val)
			p.der = insertar(p.der, x);

		return p;  // No se admiten duplicados.
	}
	
	public boolean pertenece(Integer x) 
	{
		return pertenece(raiz, x);
	}
	
	protected boolean pertenece(Nodo p, Integer x) 
	{
		if(p == null)
			return false;

		if (x.compareTo(p.val) == 0)
			return true;

		if (x.compareTo(p.val) < 0)
			return pertenece(p.izq, x);
		else
			return pertenece(p.der, x);
	}
	
	public void eliminar(Integer x) 
	{
		raiz = eliminar(raiz, x);
	}
	
	private  Nodo eliminar(Nodo p, Integer x) 
	{
		if (p == null)
			return null;

		if (x.compareTo(p.val) < 0)
			// Intentamos eliminar por la izquierda.
			p.izq = eliminar(p.izq, x);

		else if (x.compareTo(p.val) > 0)
			// Intentamos eliminar por la derecha.
			p.der = eliminar(p.der, x);

		else {
			// x == p.val: borraremos el nodo.
			tam--;

			// Si "p" es hoja o solo tiene un hijo, es fácil: sin
			// recursión, se devuelve directamente la otra rama, o null.
			if (p.izq == null && p.der == null)
				return null;

			if (p.izq == null)
				return p.der;

			if (p.der == null)
				return p.izq;

			// Caso difícil: "p" tiene dos hijos. Buscamos el mayor
			// descendiente por la izquierda (podría ser también el
			// menor por la derecha).
			p.val = maxVal(p.izq);

			// Para borrar ese "mayor descendiente por la izquierda",
			// invocamos recursivamente *una vez*.
			p.izq = eliminar(p.izq, p.val);
		}

		return p;
	}
	
	private boolean esHoja(Nodo p) 
	{
		return (p.izq == null && p.der == null);
	}
	
	private int maxVal(Nodo p) {
		// Pre-condición: p != null.
		while (p.der != null)
			p = p.der;
		return p.val;
	}
	
	public int cantidad() 
	{
		return tam;
	}
	
	//metodos obligatorios

	public int numHojas()
	{
		return numHojas(raiz);
	}
	
	private static int numHojas(Nodo p)
	{
		if (p == null)
			return 0;
		
		else if (p.izq == null && p.der == null)
			return 1;
			
		else
			return numHojas(p.izq) + numHojas(p.der);
	}
	
	public static int altura()
	{
		return altura(raiz);
	}
	
	private static int altura(Nodo p)
	{
		if (p == null)
			return 0;
		
		int izq = altura(p.izq);
		int der = altura(p.der);
		
		return 1 + Math.max(izq, der);
	}
	
	public void extraerOrdenados(Collection<Integer> dest)
	{
		extraerOrdenados(raiz, dest);
	}
	
	private void extraerOrdenados(Nodo p, Collection<Integer> dest)
	{
		if (p == null)
			return;
		
		if (p.izq != null && p.der != null)
		{		
			extraerOrdenados(p.izq, dest);
			dest.add(p.val);
			extraerOrdenados(p.der, dest);		
		}
		
		else if (p.izq != null && p.der == null)
		{
			extraerOrdenados(p.izq, dest);
			dest.add(p.val);
		}
		
		else if (p.izq == null && p.der != null)
		{
			dest.add(p.val);
			extraerOrdenados(p.der, dest);
		}
		
		else
			dest.add(p.val);
	}
	
	public void extraerPorNivel(Collection<Integer> dest)
	{
		HashMap<Integer,ArrayList<Integer>> elems = new HashMap<Integer,ArrayList<Integer>>();
		
		Integer nivel = 1;
		
		extraerPorNivel(raiz, elems, nivel);
		
		for(Integer i : elems.keySet())
		{
			dest.addAll(elems.get(i));
		}
		
	}
	
	private void extraerPorNivel(Nodo p ,HashMap<Integer,ArrayList<Integer>> elems, Integer nivel)
	{
		if (p==null)
			return;
		
		if(!elems.containsKey(nivel))
		{
			elems.put(nivel, new ArrayList<Integer>());
			elems.get(nivel).add(p.val);
		}
		
		else
		{
			ArrayList <Integer>l = elems.get(nivel);
			l.add(p.val);
			elems.put(nivel, l);
		}
		
		nivel++;	
		
		extraerPorNivel(p.izq, elems, nivel);
		extraerPorNivel(p.der, elems, nivel);
		
	}
	
	public boolean perteneceIter(int x)
	{
		Nodo n = raiz;
		
		while (n != null)
		{
			if (n.val == x)
				return true;
			
			else if (x<n.val)
				n = n.izq;
			
			else if (x>n.val)
				n = n.der;
		}
		
		return false;
		
	}
	
	public void insertarIter(int x) 
	{
		Nodo inser = new Nodo(x);
		Nodo n = raiz;
		Nodo ant = raiz;
	
		if (!pertenece(x))
			tam++;
		
		if(raiz == null)
			raiz = inser;
		
		if(!pertenece(x))
		{
			while (n != null)
			{
				if (n.val == x)
					return;

				else if (x<n.val)
				{
					ant = n;
					n = n.izq;
				}
				else if (x>n.val)
				{
					ant = n;
					n = n.der;
				}
			}

			if(x<ant.val)
				ant.izq = inser;

			else if (x>ant.val)
				ant.der = inser;		
		}
			
	}
	
	public boolean balanceado() 
	{
		if (raiz == null)
			return true;
		
		return balanceado(raiz);
	}
	
	private boolean balanceado(Nodo p)
	{
		if (p.izq==null && p.der==null)
			return true;
		
		else if (p.izq==null && altura(p.der) <= 1)
			return true;
		
		else if (p.der==null && altura(p.izq) <= 1)
			return true;
		
		else if (Math.abs(altura(p.izq) - altura(p.der)) <= 1)
			return balanceado(p.izq) && balanceado(p.der);
		
		else
			return false;
	}
	
	public static int nodosDeNivel(int i)
	{
		HashMap<Integer,Integer> nodos = new HashMap<Integer,Integer>();
		int nivel = 1;
		
		nodosDeNivel(raiz,nivel,nodos);
		
		if (i<=altura())
			return(nodos.get(i));
		else
			throw new RuntimeException("la altura es: " + altura());
	}
	
	private static void nodosDeNivel(Nodo p, int nivel, HashMap<Integer,Integer> nodos)
	{
		if (p == null)
			return;
		
		if(!nodos.containsKey(nivel))
			nodos.put(nivel, 1);
		
		else
			nodos.put(nivel, nodos.get(nivel) + 1);
		
		nivel++;	
		
		nodosDeNivel(p.izq, nivel, nodos);
		nodosDeNivel(p.der, nivel, nodos);
	}
	
	public void copiarHijos(Nodo p, int x)
	{
		
		if(p == null)
			return;
		
		if(p.val == x)
		{
//			raiz.val = p.val;
//			raiz.izq = p.izq;
//			raiz.der = p.der;
			raiz = p;
		}
		
		copiarHijos(p.izq, x);
		copiarHijos(p.der, x);
	}
	
	public Nodo getRaiz()
	{
		return raiz;
	}
	
	public static void main(String[] args) 
	{
		AbbAlumno ab = new AbbAlumno();
		AbbAlumno a = new AbbAlumno();

		
		ab.insertar(10);
		ab.insertar(2);
		ab.insertar(20);
		ab.insertar(1);
		ab.insertar(5);
		ab.insertar(12);
		ab.insertar(50);
		ab.insertar(16);
		
		System.out.println(ab.toString());
		
		a.copiarHijos(ab.getRaiz(), 2);
		
		System.out.println(a.toString());
		
		
	}

}
