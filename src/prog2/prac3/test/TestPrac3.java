package prog2.prac3.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import prog2.prac3.AbbAlumno;

public class TestPrac3
{
	private AbbAlumno ab;
	private Collection<Integer> ordenados, niveles;

	public TestPrac3() {
		ab = new AbbAlumno();
		niveles = new ArrayList<>();
		ordenados = new LinkedList<>();
	}

	@SuppressWarnings("static-access")
	@Test
	public void completo() {
		List<Integer> elems = Arrays.asList(new Integer[] {4, 2, 3, 6, 5, 7, 1});
		insertarIter(elems);

		assertEquals(7, ab.cantidad());
		assertEquals("AB<(4 (2 1 3) (6 5 7))>", ab.toString());

		assertTrue(ab.perteneceIter(4));
		assertTrue(ab.perteneceIter(7));
		assertFalse(ab.perteneceIter(8));

		assertTrue(ab.balanceado());
		assertEquals(3, ab.altura());
		assertEquals(4, ab.numHojas());

		ab.extraerPorNivel(niveles);
		ab.extraerOrdenados(ordenados);

		Collections.sort(elems);
		assertEquals(elems, ordenados);
		assertEquals(Arrays.asList(new Integer[] {4, 2, 6, 1, 3, 5, 7}), niveles);
	}

	@Test
	public void incompleto() {
		List<Integer> elems = Arrays.asList(
				new Integer[] {8, 12, 4, 14, 2, 6, 10, 3, 5, 7, 9, 11, 15, 17});
		insertarIter(elems);

		assertEquals(14, ab.cantidad());
		assertEquals(
				"AB<(8 (4 (2 \u2205 3) (6 5 7)) (12 (10 9 11) (14 \u2205 (15 \u2205 17))))>",
				ab.toString());

		assertTrue(ab.perteneceIter(9));
		assertTrue(ab.perteneceIter(14));
		assertFalse(ab.perteneceIter(1));
		assertFalse(ab.perteneceIter(16));

		assertFalse(ab.balanceado());
		assertEquals(5, ab.altura());
		assertEquals(6, ab.numHojas());

		ab.extraerPorNivel(niveles);
		ab.extraerOrdenados(ordenados);

		Collections.sort(elems);
		assertEquals(elems, ordenados);

		assertEquals(Arrays.asList(
				new Integer[] {8, 4, 12, 2, 6, 10, 14, 3, 5, 7, 9, 11, 15, 17}),
				niveles);
	}

	@Test
	public void degenerado() {
		List<Integer> elems = Arrays.asList(new Integer[] {1, 5, 10, 9, 8, 7});
		insertarIter(elems);

		assertEquals(6, ab.cantidad());
		assertEquals("AB<(1 \u2205 (5 \u2205 (10 (9 (8 7 \u2205) \u2205) \u2205)))>", ab.toString());

		assertTrue(ab.perteneceIter(5));
		assertTrue(ab.perteneceIter(7));
		assertFalse(ab.perteneceIter(4));

		assertFalse(ab.balanceado());
		assertEquals(6, ab.altura());
		assertEquals(1, ab.numHojas());

		ab.extraerPorNivel(niveles);
		ab.extraerOrdenados(ordenados);

		Collections.sort(elems);
		assertEquals(elems, ordenados);

		assertEquals(Arrays.asList(new Integer[] {1, 5, 10, 9, 8, 7}), niveles);
	}

	@Test
	public void duplicados() {
		List<Integer> elems = Arrays.asList(new Integer[] {2, 3, 2, 1, 1, 3});
		insertarIter(elems);

		assertEquals(3, ab.cantidad());
		assertEquals("AB<(2 1 3)>", ab.toString());

		assertTrue(ab.balanceado());
		assertEquals(2, ab.altura());
		assertEquals(2, ab.numHojas());

		ab.extraerPorNivel(niveles);
		ab.extraerOrdenados(ordenados);

		assertEquals(Arrays.asList(new Integer[] {2, 1, 3}), niveles);
		assertEquals(Arrays.asList(new Integer[] {1, 2, 3}), ordenados);
	}

	@Test
	public void raiz() {
		ab.insertar(10);

		assertTrue(ab.balanceado());
		assertEquals(1, ab.numHojas());

		ab.extraerPorNivel(niveles);
		ab.extraerOrdenados(ordenados);

		assertEquals(1, niveles.size());
		assertEquals(niveles, ordenados);
	}

	@Test
	public void vacio() {
		assertTrue(ab.balanceado());
		assertEquals(0, ab.cantidad());
		assertEquals(0, ab.altura());
		assertEquals(0, ab.numHojas());

		ab.extraerPorNivel(niveles);
		ab.extraerOrdenados(ordenados);

		assertTrue(niveles.isEmpty());
		assertTrue(ordenados.isEmpty());
	}

	private void insertarIter(Collection<Integer> nodos) {
		for (int x : nodos)
			ab.insertarIter(x);
	}
}