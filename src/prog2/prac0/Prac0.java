package prog2.prac0;

public class Prac0 
{

	public static int[] combinarOrdenados(int[] a, int[] b)
	{
		int[] res = new int[a.length + b.length];
		int k = 0;
		int j = 0;

		for (int i=0; i<a.length; i++)
		{
			while (j<b.length)
			{
				if(a[i]>=b[j])
				{
					res[k]=b[j];
					j++;
					k++;
				}
				
				else
				{
					res[k] = a[i];
					k++;
					break;
				}
			}
			if(j==b.length)
			{
				res[k]=a[i];
				k++;
			}
		}
		while(j<b.length)
		{
			res[k]=b[j];
			j++;
			k++;
		}
		return res;
	}
	
	public static boolean pertenecenTodos (int[]elems, int[]arreglo)
	{
		if (elems.length==0)
			return true;
		
		boolean res = true;
		
		for (int i : elems)
		{
			res = res && pertenece(i,arreglo);
		}
		
		return res;
	}
	
	private static boolean pertenece (int i, int[] lista)
	{
		boolean res = false;
		
		for (int j : lista)
		{
			if (i == j)
				res = true;
		}
		return res;
	}
	
	public static int indicePico(int[] arreglo)
	{
		
		if (arreglo.length<3 || PosiblePico(arreglo) == 0)
			return -1;

		else if (sigueDecreciente(arreglo,PosiblePico(arreglo)))
			return PosiblePico(arreglo);
		
		else
			return -1;
		
		
	}
	
	private static int PosiblePico(int[] a)
	{
		int res = 0;
		for (int i=0; i<a.length-1; i++)
		{
			if(a[i]>=a[i+1])
			{
				res=i;
				break;
			}
		}
		return res;
	}
	
	private static boolean sigueDecreciente(int[] a, int x)
	{
		boolean res = false;
		for ( int i = x; i<a.length-1; i++)
		{
			if ( a[i]>a[i+1])
				res = true;
			else
			{
				res = false;
				break;
			}
		}
		return res;
	}
		
	
	public static int indicePicoLog(int[] arreglo)
	{
		return -42;
	}
}
